
package br.ufrn.sipac.almoxarifado.dominio;

import java.math.BigDecimal;
import java.util.Date;

public class NotaFiscal {

   private Integer id = null;
   private String numero = null;
   private String serie = null;
   private BigDecimal valor = null;
   private String fornecedor = null;
   private Date dataCadastro = null;
   private String observacao = null;
   private String empenho = null;

   public NotaFiscal() {
      super();
   }

   public Date getDataCadastro() {
      return dataCadastro;
   }

   public String getEmpenho() {
      return empenho;
   }

   public String getFornecedor() {
      return fornecedor;
   }

   public Integer getId() {
      return id;
   }

   public String getNumero() {
      return numero;
   }

   public String getObservacao() {
      return observacao;
   }

   public String getSerie() {
      return serie;
   }

   public BigDecimal getValor() {
      return valor;
   }

   public void setDataCadastro(Date dataCadastro) {
      this.dataCadastro = dataCadastro;
   }

   public void setEmpenho(String empenho) {
      this.empenho = empenho;
   }

   public void setFornecedor(String fornecedor) {
      this.fornecedor = fornecedor;
   }

   public void setId(Integer id) {
      this.id = id;
   }
   
   public void setNumero(String numero) {
      this.numero = numero;
   }

   public void setObservacao(String observacao) {
      this.observacao = observacao;
   }

   public void setSerie(String serie) {
      this.serie = serie;
   }

   public void setValor(BigDecimal valor) {
      this.valor = valor;
   }

}
