
package br.ufrn.sipac.almoxarifado.repository;

import java.util.ArrayList;
import java.util.Collection;
import br.ufrn.sipac.almoxarifado.dominio.NotaFiscal;

public class NotaFiscalRepository {

   public void atualiza(NotaFiscal nota) {

   }

   public void exclui(NotaFiscal nota) {

   }

   public Collection<NotaFiscal> listaTodos() {
      return new ArrayList<NotaFiscal>();
   }

   public void mescla(NotaFiscal nota) {

   }

   public NotaFiscal pega(String numero) {
      return new NotaFiscal();
   }

   public void salva(NotaFiscal nota) {

   }

   public NotaFiscal pegaPorId(Integer id) {
      return new NotaFiscal();
   }

   public NotaFiscal metodo() {
      return new NotaFiscal();
   }

}
